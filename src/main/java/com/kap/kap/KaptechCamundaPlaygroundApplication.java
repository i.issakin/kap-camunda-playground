package com.kap.kap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KaptechCamundaPlaygroundApplication {

    public static void main(String[] args) {
        SpringApplication.run(KaptechCamundaPlaygroundApplication.class, args);
    }

}
