package com.kap.kap.web;

import lombok.RequiredArgsConstructor;
import org.camunda.bpm.engine.RuntimeService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@RestController
@RequestMapping("/input")
@RequiredArgsConstructor
public class ReceiveTaskController {

    private final RuntimeService runtimeService;

    @PostMapping("/submit")
    public ResponseEntity<?> submit(@RequestParam String bKey, @RequestParam String messageName, @RequestBody HashMap<String, Object> params) {

        var correlation = runtimeService.createMessageCorrelation(messageName);
        correlation.setVariables(params);
        correlation.processInstanceBusinessKey(bKey);
        var result = correlation.correlateWithResult();

        return ResponseEntity.ok().build();
    }

}
