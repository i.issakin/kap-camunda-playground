package com.kap.kap.web;

import lombok.RequiredArgsConstructor;
import org.camunda.bpm.engine.RuntimeService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping("/process")
@RequiredArgsConstructor
public class ProcessController {

    private final RuntimeService runtimeService;

    @PostMapping("/start/{definitionKey}")
    public ResponseEntity<?> start(@PathVariable("definitionKey") String definitionKey) {
        UUID bKey = UUID.randomUUID();
        runtimeService.startProcessInstanceByKey(definitionKey, bKey.toString());
        return ResponseEntity.ok(bKey);
    }

}
