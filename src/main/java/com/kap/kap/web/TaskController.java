package com.kap.kap.web;

import com.kap.kap.bpm.BpmTask;
import com.kap.kap.bpm.TaskDto;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/task")
@RequiredArgsConstructor
public class TaskController {

    private final List<BpmTask> tasks;

    @SneakyThrows
    @GetMapping("/service")
    public ResponseEntity<List<TaskDto>> getTasks() {

        return ResponseEntity.ok(tasks.stream()
                .map(it -> new TaskDto(
                        it.getName(),
                        it.getDescription(),
                        it.getComponentCode(),
                        it.getMessageName(),
                        it.getType(),
                        Arrays.asList(it.getRequiredInputs()),
                        Arrays.asList(it.getExpectedOutputs())
                ))
                .collect(Collectors.toList()));
    }

}
