package com.kap.kap.web;

import com.kap.kap.service.DefinitionService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/definition")
@RequiredArgsConstructor
public class DefinitionController {

    private final DefinitionService definitionService;

    @PostMapping("/upload")
    public ResponseEntity<?> uploadDefinition(@RequestParam String name, @RequestBody String bpmnBytes, HttpServletRequest request) {
        definitionService.uploadDefinition(name, bpmnBytes);
        return ResponseEntity.ok().build();
    }

}
