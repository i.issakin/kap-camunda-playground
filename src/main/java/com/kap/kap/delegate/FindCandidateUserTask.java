package com.kap.kap.delegate;

import com.kap.kap.bpm.BpmTask;
import com.kap.kap.bpm.TaskType;
import com.kap.kap.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Component("findCandidateUserTask")
@RequiredArgsConstructor
public class FindCandidateUserTask extends BpmTask implements JavaDelegate {

    private final UserService userService;

    @Override
    public String getName() {
        return "Назначить ответственного за устранение инцидента";
    }

    @Override
    public String getDescription() {
        return "По типу инцидента определяет группу пользователей и находит в ней одного с подходящим расписанием";
    }

    @Override
    public String getComponentCode() {
        return "findCandidateUserTask";
    }

    @Override
    public TaskType getType() {
        return TaskType.SERVICE;
    }

    @Override
    public String[] getRequiredInputs() {
        return new String[]{
                "incidentType"
        };
    }

    @Override
    public String[] getExpectedOutputs() {
        return new String[]{
                "userId"
        };
    }

    @Override
    public String getMessageName() {
        return null;
    }

    @Override
    public void execute(DelegateExecution execution) throws Exception {
        Map<String, String> requiredVars = new HashMap<>();
        for (String requiredInput : getRequiredInputs()) {
            var value = execution.getVariable(requiredInput);
            if (value == null) {
                throw new RuntimeException(String.format("Missing %s value", requiredInput));
            }
            requiredVars.put(requiredInput, value.toString());
        }

        var candidateUser = userService.getCandidate(requiredVars.get("incidentType"));

        execution.setVariable("userId", candidateUser);

    }
}
