package com.kap.kap.delegate;

import com.kap.kap.bpm.BpmTask;
import com.kap.kap.bpm.TaskType;
import org.springframework.stereotype.Component;

@Component("fixIncidentTask")
public class FixIncidentTask extends BpmTask {

    @Override
    public String getName() {
        return "Устранить причину и последствия инцидента";
    }

    @Override
    public String getDescription() {
        return "Назначенный ранее пользователь разбирается с инцидентом";
    }

    @Override
    public String getComponentCode() {
        return "fixIncidentTask";
    }

    @Override
    public TaskType getType() {
        return TaskType.USER;
    }

    @Override
    public String[] getRequiredInputs() {
        return new String[]{
                "userId"
        };
    }

    @Override
    public String[] getExpectedOutputs() {
        return new String[]{
                "timeSpent"
        };
    }

    @Override
    public String getMessageName() {
        return null;
    }
}
