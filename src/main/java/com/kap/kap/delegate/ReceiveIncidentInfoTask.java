package com.kap.kap.delegate;

import com.kap.kap.bpm.BpmTask;
import com.kap.kap.bpm.TaskType;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component("receiveIncidentInfoTask")
@RequiredArgsConstructor
public class ReceiveIncidentInfoTask extends BpmTask {

    @Override
    public String getName() {
        return "Получить данные об инциденте";
    }

    @Override
    public String getDescription() {
        return "Процесс ждёт тип и описание инцидента";
    }

    @Override
    public String getComponentCode() {
        return "receiveIncidentInfoTask";
    }

    @Override
    public TaskType getType() {
        return TaskType.RECEIVE;
    }

    @Override
    public String[] getRequiredInputs() {
        return new String[]{
                "incidentType",
                "incidentDescription"
        };
    }

    @Override
    public String[] getExpectedOutputs() {
        return new String[0];
    }

    @Override
    public String getMessageName() {
        return "Message_IncidentInfoReceived";
    }
}
