package com.kap.kap.bpm;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TaskDto {

    private String name;

    private String description;

    private String componentCode;

    private String messageName;

    private TaskType type;

    private List<String> requiredFields;

    private List<String> outputs;
}
