package com.kap.kap.bpm;

public abstract class BpmTask {


    public abstract String getName();

    public abstract String getDescription();

    public abstract String getComponentCode();

    public abstract TaskType getType();

    public abstract String[] getRequiredInputs();

    public abstract String[] getExpectedOutputs();

    // ----

    public abstract String getMessageName();

}
