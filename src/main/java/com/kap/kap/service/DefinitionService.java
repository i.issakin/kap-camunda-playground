package com.kap.kap.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.RepositoryService;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class DefinitionService {

    private final RepositoryService repositoryService;

    public void uploadDefinition(String name, String bpmnBytes) {
        org.camunda.bpm.engine.repository.Deployment deployment = repositoryService.createDeployment()
                .addString(name + ".bpmn", String.valueOf(bpmnBytes))
                .deploy();

        log.debug(deployment.getId());
    }
}
