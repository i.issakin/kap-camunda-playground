package com.kap.kap.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class UserService {

    public String getCandidate(String incidentType) {
        switch (incidentType) {
            case "ALPHA":
                return "1";
            case "OMEGA":
                return "2";
            default:
                return "3";
        }
    }
}
